package org.example;

import org.example.window.ProductWindow;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame productFrame = new JFrame("FormProduct");
        productFrame.setSize(new Dimension(800,800));
        productFrame.setContentPane(new ProductWindow().getMainPanel());
        productFrame.setVisible(true);
    }
}