package org.example.window;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class PanierWindow {
    private JPanel mainPanel;
    private GridLayout mainGridLayout;

    private JPanel formPanel;

    private JPanel buttonPanel;
    private GridBagLayout formGridLayout;
    private GridBagConstraints formBagConstraints;

    private JTextField idTextField;
    private JTextField nameTextField;
    private JTextArea descriptionTextArea;
    private JTextArea quantityTextArea;
    private JTextArea priceTextArea;

    private JButton addButton;
    private JButton updateButton;
    private JButton firstButton;
    private JButton previousButton;
    private JButton nextButton;
    private JButton lastButton;

    private ButtonGroup buttonGroup1;
    private ButtonGroup buttonGroup2;
    private List<String> labels = Arrays.asList("ID", "NAME", "DESCRIPTION", "QTY", "PRICE");

    public PanierWindow() {
//        productRepository = new ProductRepository();
//        productService = new ProductService(productRepository);
        init();
    }

    private void init() {
        mainGridLayout = new GridLayout(1,2);
        mainPanel = new JPanel();
        mainPanel.setLayout(mainGridLayout);

        mainPanel.setBorder(BorderFactory.createTitledBorder("Formulaire Product"));

        createForm();
    }

    private void createForm() {
        formPanel = new JPanel();
        formGridLayout = new GridBagLayout();
        formBagConstraints = new GridBagConstraints();
        formPanel.setLayout(formGridLayout);
        mainPanel.add(formPanel);
        createLabelForm();
        createFieldForm();
    }

    private void createLabelForm() {
        int y = 0;
        for(String s: labels) {
            Label l = new Label(s);
            formBagConstraints.weightx = 0.5;
            formBagConstraints.weighty = (s.equals("ID")) ? 1 : 0.5;
            formBagConstraints.gridy = y++;
            formBagConstraints.gridx = 0;
            formPanel.add(l, formBagConstraints);
        }
    }


    private void createFieldForm() {
        formBagConstraints.gridx = 1;
        formBagConstraints.weightx = 1;
        formBagConstraints.fill = GridBagConstraints.BOTH;


        idTextField = new JTextField("");
        formBagConstraints.gridy = 0;
        formPanel.add(idTextField, formBagConstraints);

        nameTextField = new JTextField("");
        formBagConstraints.gridy = 1;
        formPanel.add(nameTextField, formBagConstraints);

        descriptionTextArea = new JTextArea("");
        formBagConstraints.gridy = 2;
        formPanel.add(descriptionTextArea, formBagConstraints);

        quantityTextArea = new JTextArea("");
        formBagConstraints.gridy = 3;
        formPanel.add(quantityTextArea, formBagConstraints);

        priceTextArea = new JTextArea("");
        formBagConstraints.gridy = 4;
        formPanel.add(priceTextArea, formBagConstraints);

        buttonPanel = new JPanel();

        buttonGroup1 = new ButtonGroup();
        addButton = new JButton("add");
        updateButton = new JButton("update");

        buttonGroup2 = new ButtonGroup();
        firstButton = new JButton("first");
        previousButton = new JButton("previous");
        nextButton = new JButton("next");
        lastButton = new JButton("last");

        buttonPanel.setLayout(new FlowLayout());
        buttonGroup1.add(addButton);
        buttonGroup1.add(updateButton);

        buttonGroup2.add(firstButton);
        buttonGroup2.add(previousButton);
        buttonGroup2.add(nextButton);
        buttonGroup2.add(lastButton);

        formBagConstraints.gridy = 4;
        formBagConstraints.fill = GridBagConstraints.CENTER;
        formPanel.add(buttonPanel, formBagConstraints);
    }
}
